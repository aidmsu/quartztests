﻿using System;
using System.Threading.Tasks;
using Quartz;

namespace QuartzTest
{
    public class QuartzScheduleJobManager
    {
        private readonly AbpQuartzConfiguration _quartzConfiguration;

        public QuartzScheduleJobManager(
            AbpQuartzConfiguration quartzConfiguration)
        {
            _quartzConfiguration = quartzConfiguration;
            _quartzConfiguration.Scheduler.Start();
        }

        public async Task ScheduleAsync<TJob>(Action<JobBuilder> configureJob, Action<TriggerBuilder> configureTrigger)
            where TJob : IJob
        {
            var jobToBuild = JobBuilder.Create<TJob>();
            configureJob(jobToBuild);
            var job = jobToBuild.Build();

            var triggerToBuild = TriggerBuilder.Create();
            configureTrigger(triggerToBuild);
            var trigger = triggerToBuild.Build();

            _quartzConfiguration.Scheduler.ScheduleJob(job, trigger);
            await Task.CompletedTask;
        }

        public void Schedule<TJob>(Action<JobBuilder> configureJob, Action<TriggerBuilder> configureTrigger)
            where TJob : IJob
        {
            var jobToBuild = JobBuilder.Create<TJob>();
            configureJob(jobToBuild);
            var job = jobToBuild.Build();

            var triggerToBuild = TriggerBuilder.Create();
            configureTrigger(triggerToBuild);
            var trigger = triggerToBuild.Build();

            _quartzConfiguration.Scheduler.ScheduleJob(job, trigger);
        }
    }
}