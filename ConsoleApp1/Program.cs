﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using QuartzTest;

namespace ConsoleApp1
{
    class Program
    {
        public static QuartzScheduleJobManager _quartzScheduleJobManager;

        static void Main(string[] args)
        {
            _quartzScheduleJobManager = new QuartzScheduleJobManager(new AbpQuartzConfiguration());

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var a = ScheduleJobs().Result;

            Console.WriteLine(stopwatch.Elapsed);
            Console.ReadLine();
        }

        private static async Task<bool> ScheduleJobs()
        {
            await _quartzScheduleJobManager.ScheduleAsync<HelloJob>(
                job =>
                {
                    job.WithDescription("HelloJobDescription")
                        .WithIdentity("HelloJobKey");
                },
                trigger =>
                {
                    trigger.WithIdentity("HelloJobTrigger")
                        .WithDescription("HelloJobTriggerDescription")
                        .WithSimpleSchedule(schedule => schedule.WithRepeatCount(5).WithInterval(TimeSpan.FromSeconds(1)))
                        .StartNow();
                });

            await _quartzScheduleJobManager.ScheduleAsync<GoodByeJob>(
                job =>
                {
                    job.WithDescription("GoodByeJobDescription")
                        .WithIdentity("GoodByeJobKey");
                },
                trigger =>
                {
                    trigger.WithIdentity("GoodByeJobTrigger")
                        .WithDescription("GoodByeJobTriggerDescription")
                        .WithSimpleSchedule(schedule => schedule.WithRepeatCount(5).WithInterval(TimeSpan.FromSeconds(1)))
                        .StartNow();
                });

            return true;
        }

        [DisallowConcurrentExecution]
        public class HelloJob : JobBase
        {
            private readonly IHelloDependency _helloDependency;

            //public HelloJob(IHelloDependency helloDependency)
            //{
            //    _helloDependency = helloDependency;
            //}

            public override Task Execute(IJobExecutionContext context)
            {
                //_helloDependency.ExecutionCount++;
                return Task.CompletedTask;
            }
        }

        [DisallowConcurrentExecution]
        public class GoodByeJob : JobBase
        {
            private readonly IGoodByeDependency _goodByeDependency;

            //public GoodByeJob(IGoodByeDependency goodByeDependency)
            //{
            //    _goodByeDependency = goodByeDependency;
            //}

            public override Task Execute(IJobExecutionContext context)
            {
                //_goodByeDependency.ExecutionCount++;
                return Task.CompletedTask;
            }
        }

        public interface IHelloDependency
        {
            int ExecutionCount { get; set; }
        }

        public interface IGoodByeDependency
        {
            int ExecutionCount { get; set; }
        }

        public class HelloDependency : IHelloDependency//, ISingletonDependency
        {
            public int ExecutionCount { get; set; }
        }

        public class GoodByeDependency : IGoodByeDependency//, ISingletonDependency
        {
            public int ExecutionCount { get; set; }
        }

        public abstract class JobBase : IJob
        {


            /// <summary>
            ///     Gets/sets name of the localization source that is used in this application service.
            ///     It must be set in order to use <see cref="L(string)" /> and <see cref="L(string,CultureInfo)" /> methods.
            /// </summary>
            protected string LocalizationSourceName { get; set; }

            public abstract Task Execute(IJobExecutionContext context);





        }
    }
}
