﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

namespace QuartzTest
{
    class Program
    {
        public static QuartzScheduleJobManager _quartzScheduleJobManager;

        static void Main(string[] args)
        {
            _quartzScheduleJobManager = new QuartzScheduleJobManager(new AbpQuartzConfiguration());

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            ScheduleJobs();

            Console.WriteLine(stopwatch.Elapsed);
            Console.ReadLine();
        }

        private static void ScheduleJobs()
        {
            _quartzScheduleJobManager.Schedule<HelloJob>(
                job =>
                {
                    job.WithDescription("HelloJobDescription")
                        .WithIdentity("HelloJobKey");
                },
                trigger =>
                {
                    trigger.WithIdentity("HelloJobTrigger")
                        .WithDescription("HelloJobTriggerDescription")
                        .WithSimpleSchedule(schedule => schedule.WithRepeatCount(5).WithInterval(TimeSpan.FromSeconds(1)))
                        .StartNow();
                });

            _quartzScheduleJobManager.Schedule<GoodByeJob>(
                job =>
                {
                    job.WithDescription("GoodByeJobDescription")
                        .WithIdentity("GoodByeJobKey");
                },
                trigger =>
                {
                    trigger.WithIdentity("GoodByeJobTrigger")
                        .WithDescription("GoodByeJobTriggerDescription")
                        .WithSimpleSchedule(schedule => schedule.WithRepeatCount(5).WithInterval(TimeSpan.FromSeconds(1)))
                        .StartNow();
                });
        }

        [DisallowConcurrentExecution]
        public class HelloJob : JobBase
        {
            private readonly IHelloDependency _helloDependency;

            public HelloJob()
            {
                //_helloDependency = helloDependency;
            }

            public override void Execute(IJobExecutionContext context)
            {
                //_helloDependency.ExecutionCount++;
                //Console.WriteLine("Hi");
            }
        }

        [DisallowConcurrentExecution]
        public class GoodByeJob : JobBase
        {
            private readonly IGoodByeDependency _goodByeDependency;

            public GoodByeJob()
            {
            }

            public override void Execute(IJobExecutionContext context)
            {
                //_goodByeDependency.ExecutionCount++;
                //Console.WriteLine("Hi1");

            }
        }

        public interface IHelloDependency
        {
            int ExecutionCount { get; set; }
        }

        public interface IGoodByeDependency
        {
            int ExecutionCount { get; set; }
        }

        public class HelloDependency : IHelloDependency//, ISingletonDependency
        {
            public int ExecutionCount { get; set; }
        }

        public class GoodByeDependency : IGoodByeDependency//, ISingletonDependency
        {
            public int ExecutionCount { get; set; }
        }

        public abstract class JobBase : IJob
        {
           

            /// <summary>
            ///     Gets/sets name of the localization source that is used in this application service.
            ///     It must be set in order to use <see cref="L(string)" /> and <see cref="L(string,CultureInfo)" /> methods.
            /// </summary>
            protected string LocalizationSourceName { get; set; }

            public abstract void Execute(IJobExecutionContext context);

           

           

        }
    }
}
