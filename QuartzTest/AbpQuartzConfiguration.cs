﻿using Quartz;
using Quartz.Impl;

namespace QuartzTest
{
    public class AbpQuartzConfiguration
    {
        public IScheduler Scheduler => StdSchedulerFactory.GetDefaultScheduler();
    }
}